package com.winio94.game.Vehicles;
/**
 * 
 * @author Michal Winnicki
 *
 */
public class Motocycle extends Vehicle {

	private String model;
	private int distance;
	private boolean heatedManets;
	/**
	 * 
	 * @param speed predkosc
	 * @param acceleration
	 * @param colour
	 * @param model
	 * @param distance
	 * @param heatedManets
	 */
	public Motocycle(int speed, int acceleration, String colour, String model,
			int distance, boolean heatedManets) {
		super(speed, acceleration, colour);
		this.model = model;
		this.distance = distance;
		this.heatedManets = heatedManets;
	}

	@Override
	/**
	 * 
	 */
	public void drive() {
		System.out.println(" by Motocycle");

	}
	/**
	 * 
	 * @return model
	 */ 
	public String getModel() {
		return model;
	}
	/**
	 * 
	 * @return dystans
	 */
	public int getDistance() {
		return distance;
	}
	/**
	 * 
	 * @return podgrzewane manetki
	 */
	public boolean isHeatedManets() {
		return heatedManets;
	}

	/**
	 * nadpisana metoda hamuj
	 */
	public void Break() {
		// TODO Auto-generated method stub
		
	}

}
