package com.winio94.game.Vehicles;

/**
 * 
 * @author Michal Winnicki
 * 
 */
public class Car extends Vehicle {
	private int trunkSize;
	private String type;
	private int howManyPeople;

	/**
	 * 
	 * @param speed
	 *            predkosc samochodu
	 * @param acceleration
	 *            przyspieszenie
	 * @param colour
	 *            kolor
	 * @param trunkSize
	 *            wielkosc bagaznika
	 * @param type
	 *            model
	 * @param howManyPeople
	 *            wielkosc
	 */
	public Car(int speed, int acceleration, String colour, int trunkSize,
			String type, int howManyPeople) {
		super(speed, acceleration, colour);
		this.trunkSize = trunkSize;
		this.type = type;
		this.howManyPeople = howManyPeople;
	}

	@Override
	/**
	 * nadpisana metoda abstrakcyjna
	 */
	public void drive() {
		System.out.println(" by Car");

	}

	/**
	 * 
	 * @return zwraca wielkosc bagaznika
	 */
	public int getTrunkSize() {
		return trunkSize;
	}

	/**
	 * 
	 * @return zwraca typ
	 */
	public String getType() {
		return type;
	}

	/**
	 * 
	 * @return zwraca ilosc osob
	 */
	public int getHowManyPeople() {
		return howManyPeople;
	}

	/**
	 * nadpisana metoda hamuj
	 */
	public void Break() {
		

	}

}
