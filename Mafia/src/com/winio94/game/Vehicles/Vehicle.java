package com.winio94.game.Vehicles;
/**
 * 
 * @author Michal Winnicki
 *
 */
public abstract class Vehicle {

	protected int speed;
	protected int acceleration;
	protected String colour;
	/**
	 * 
	 * @param speed
	 * @param acceleration
	 * @param colour
	 */
	public Vehicle(int speed, int acceleration, String colour) {
		this.speed = speed;
		this.acceleration = acceleration;
		this.colour = colour;
	}
	/**
	 * abstrakcyjna klasa jedz
	 */
	public abstract void drive();
	/**
	 * 
	 * @return predkosc
	 */
	public int getSpeed() {
		return speed;
	}
	/**
	 * 
	 * @return przyspieszenie
	 */
	public int getAcceleration() {
		return acceleration;
	}
	/**
	 * 
	 * @return kolor
	 */
	public String getColour() {
		return colour;
	}

	@Override
	public String toString() {
		return "Speed = " + getSpeed() + ", Acceleration = "
				+ getAcceleration() + ", Colour = " + getColour();
	}
	/**
	 * abstrakcyjna metoda hamuj
	 */
	public abstract void Break();
	/**
	 * przyspieszanie
	 */
	public void Accelerate() {
		speed += 10;
	}
}
