package com.winio94.game.weapons;

/**
 * 
 * @author Michl Winnicki
 * 
 */
public abstract class Weapon {
	private int damage;
	private double weight;
	private String colour;

	/**
	 * 
	 * @param damage
	 * @param weight
	 * @param colour
	 */
	public Weapon(int damage, double weight, String colour) {
		super();
		this.damage = damage;
		this.weight = weight;
		this.colour = colour;
	}

	/**
	 * abstrakcyjna metoda walcz(strzelanie lub walka mieczem)
	 */
	public abstract void Fight();
	public abstract void Repare();
	
	/**
	 * 
	 * @return zwraca obrazenia
	 */
	public int getDamage() {
		return damage;
	}

	/**
	 * 
	 * @return wage broni
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * 
	 * @return kolor
	 */
	public String getColour() {
		return colour;
	}

	@Override
	public String toString() {
		return "\nDamage = " + getDamage() + ", Weight = " + getWeight()
				+ ", Colour = " + getColour();
	}

}
