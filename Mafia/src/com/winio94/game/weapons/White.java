package com.winio94.game.weapons;
/**
 * 
 * @author Michal Winnicki
 *
 */
public class White extends Weapon {
	private boolean hasEdge;
	private int sharpeness;
	private String oneOrTwoHands;
	/**
	 * 
	 * @param damage
	 * @param weight
	 * @param colour
	 * @param hasEdge
	 * @param sharpeness
	 * @param oneOrTwoHands
	 */
	public White(int damage, double weight, String colour, boolean hasEdge,
			int sharpeness, String oneOrTwoHands) {
		super(damage, weight, colour);
		this.hasEdge = hasEdge;
		this.sharpeness = sharpeness;
		this.oneOrTwoHands = oneOrTwoHands;
	}

	@Override
	/**
	 * nadpisana metoda walcz z klasy Weapon
	 */
	public void Fight() {
		System.out.println("Fighting with white weapon!!");

	}
	/**
	 * 
	 * 
	 * @return 
	 */
	public boolean isHasEdge() {
		return hasEdge;
	}
	/**
	 * 
	 * @return jedno czy dwureczna?
	 */
	public String getOneOrTwoHands() {
		return oneOrTwoHands;
	}
	/**
	 * ostrzenie jesli ma ostrze
	 */
	public void Sharpen() {
		if (hasEdge)
			sharpeness += 10;
		else System.out.println("Ten rodzaj broni bialej nie ma ostrza!");

	}
	/**
	 * 
	 * @return ostrosc
	 */
	public int getSharpeness() {
		return sharpeness;
	}

	@Override
	public void Repare() {
		// TODO Auto-generated method stub
		
	}

}
