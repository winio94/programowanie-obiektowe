package com.winio94.game.weapons;

import com.winio94.game.exception.UnpriopriateValueException;
import com.winio94.game.exception.*;

/**
 * 
 * @author Michl Winnicki
 * 
 */
public class Riffle extends Weapon {

	private int ammo;
	private int accuracy;

	/**
	 * 
	 * @param damage
	 * @param weight
	 * @param colour
	 * @param ammo
	 * @param accuracy
	 */
	public Riffle(int damage, double weight, String colour, int ammo,
			int accuracy) {
		super(damage, weight, colour);
		this.ammo = ammo;
		this.accuracy = accuracy;
	}

	@Override
	/**
	 * nadpisana metoda abstrakcyjna z klasy Weapon
	 */
	public void Fight() {
		System.out.println("Fighting with riffle");
		shoot();
	}

	// METODY WYSTEPUJACE TYLO W KLASIE Riffle
	/**
	 * metoda strzelaj
	 */
	public void shoot() {
		ammo -= 10;
		accuracy -= 1;
	}

	/**
	 * metoda przeladuj
	 */
	public void Reload(int n) throws UnpriopriateValueException {
		if (n <= 0)
			throw new UnpriopriateValueException();
		else
			ammo += n;
	}

	/**
	 * 
	 * @return zwraca ilosc amunicji
	 */
	public int getAmmo() {
		return ammo;
	}

	/**
	 * 
	 * @return celnosc
	 */
	public int getAccuracy() {
		return accuracy;
	}

	@Override
	public void Repare() {
		// TODO Auto-generated method stub

	}

}
