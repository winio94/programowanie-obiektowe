package com.winio94.game.Characters;

import com.winio94.game.Vehicles.Vehicle;
import com.winio94.game.exception.CollectionCapacityOverflowException;
import com.winio94.game.exception.NullObjectException;
import com.winio94.game.exception.UnattackableException;
import com.winio94.game.items.Backpack;
import com.winio94.game.items.Item;
import com.winio94.game.weapons.Riffle;
import com.winio94.game.weapons.Weapon;
import com.winio94.game.weapons.White;

/**
 * 
 * @author Michal Winnicki
 * 
 */
public abstract class Character {
	private String name;
	private String sureName;
	private int height;
	private int weight;
	private int HP;// life
	private Weapon weapon;
	private Vehicle vehicle;
	
	private Backpack backPack = new Backpack();

	/**
	 * 
	 * @param name
	 * @param sureName
	 * @param height
	 * @param weight
	 * @param hP
	 * @param weapon
	 * @param vehicle
	 */
	public Character(String name, String sureName, int height, int weight,
			int hP, Weapon weapon, Vehicle vehicle) {
		this.name = name;
		this.sureName = sureName;
		this.height = height;
		this.weight = weight;
		HP = hP;
		this.weapon = weapon;
		this.vehicle = vehicle;
	}

	/**
	 * 
	 * @param weapon
	 */
	public abstract void Fight(Character character, Weapon weapon)
			throws UnattackableException, NullObjectException;

	public abstract void move();

	/**
	 * jedzenie
	 */
	public void Eat() {
		HP += 10;
		weight += 5;
	}

	/**
	 * 
	 * @param vehicle
	 *            abstrakcyjna metoda jedz
	 */
	public abstract void Drive(Vehicle vehicle);

	/**
	 * 
	 * @return imie
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return nazwisko
	 */
	public String getSureName() {
		return sureName;
	}

	/**
	 * 
	 * @return wzrost
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * 
	 * @return waga
	 * */
	public int getWeight() {
		return weight;
	}

	/**
	 * 
	 * @return hp
	 */
	public int getHP() {
		return HP;
	}

	/**
	 * 
	 * @return rodzaj broni
	 */
	public Weapon getWeapon() {
		return weapon;
	}

	/**
	 * 
	 * @return pojazd
	 */
	public Vehicle getVehicle() {
		return vehicle;
	}

	@Override
	/**
	 * metoda toString
	 */
	public String toString() {
		return "\nCharacters Name = " + getName() + ", SureName = "
				+ getSureName() + ", tHeight = " + getHeight() + ", Weight = "
				+ getWeight() + ", HP = " + getHP() + ", Weapon = "
				+ getWeapon() + ", Vehicle = " + getVehicle();
	}
	
	public void itemAdding(Item item) {
		try {
			backPack.addItem(item);
		} catch(CollectionCapacityOverflowException ex) {
			ex.noFreeSpace();
			ex.printStackTrace();
		}
		
	}
	
}
