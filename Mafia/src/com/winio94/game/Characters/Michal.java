package com.winio94.game.Characters;

import com.winio94.game.Vehicles.Car;
import com.winio94.game.Vehicles.Motocycle;
import com.winio94.game.Vehicles.Vehicle;
import com.winio94.game.exception.NullObjectException;
import com.winio94.game.exception.UnattackableException;
import com.winio94.game.weapons.Riffle;
import com.winio94.game.weapons.Weapon;
import com.winio94.game.weapons.White;

/**
 * 
 * @author Michal Winnicki
 * 
 */
public class Michal extends Character {
	/**
	 * Klasa uzywa w swojej implementacji wzorca projektowego Singleton ma to
	 * nie dopuscic do mozliwo�ci utworzenia dw�ch roznych obiektow Michal
	 * podczas jednej gry //ponizsza klasa uzywa jednej z najczesciej
	 * wykorzystywanych implementacji //Singletonu z wykorzystaniem pomocniczej
	 * klasy wewn�trznej
	 * 
	 * @param name
	 * @param sureName
	 * @param height
	 * @param weight
	 * @param hP
	 * @param weapon
	 * @param vehicle
	 */
	private Michal(String name, String sureName, int height, int weight,
			int hP, Weapon weapon, Vehicle vehicle) {
		super(name, sureName, height, weight, hP, weapon, vehicle);
		System.out.println("Character Michal Created!");
	}

	/**
	 * pomocnicza klasa wewnetrzna
	 */
	private static final class MichalHelper {
		private static final Weapon riffle = new Riffle(100, 3.76, "Black", 16,
				80);
		private static final Vehicle car = new Car(250, 7, "Red", 500,
				"Five doors", 5);
		private static final Michal INSTANCE = new Michal("Michal", "Winnicki",
				180, 80, 100, riffle, car);
	}

	/**
	 * 
	 * @return funkcja zwraca obiekt Michal
	 */
	public static Michal getMichalInstance() {
		return MichalHelper.INSTANCE;
	}

	/**
	 * nadpisana metoda jedz
	 */
	public void Drive(Vehicle vehicle) throws NullObjectException {

		if (vehicle == null)
			throw new NullObjectException();

		else {
			System.out.print("Michal driving by ");
			if (vehicle instanceof Car)
				System.out.println("car");
			else if (vehicle instanceof Motocycle)
				System.out.println("motocycle");
		}

	}

	@Override
	public void move() {
		System.out.println("Poruszanie sie Michala");

	}

	@Override
	public void Fight(Character character, Weapon weapon)
			throws UnattackableException, NullObjectException {
		if(character == null) 
			throw new NullObjectException();
		else if (character == getMichalInstance())
			throw new UnattackableException();
		else {
			System.out.print("You are fighting with " + character.getName()
					+ " using ");
			if (weapon instanceof Riffle)
				System.out.println("riffle");
			else if (weapon instanceof White)
				System.out.println("white weapon");

			weapon.Fight();
		}

	}

}
