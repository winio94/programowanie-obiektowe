package com.winio94.game.Characters;

import java.io.ObjectInputStream.GetField;

import com.winio94.game.Vehicles.Car;
import com.winio94.game.Vehicles.Motocycle;
import com.winio94.game.Vehicles.Vehicle;
import com.winio94.game.exception.NullObjectException;
import com.winio94.game.exception.UnattackableException;
import com.winio94.game.weapons.Riffle;
import com.winio94.game.weapons.Weapon;
import com.winio94.game.weapons.White;

/**
 * 
 * @author Michal Winnicki
 * 
 */
public class Dawid extends Character {
	/**
	 * 
	 * @param name
	 * @param sureName
	 * @param height
	 * @param weight
	 * @param hP
	 * @param weapon
	 * @param vehicle
	 */
	public Dawid(String name, String sureName, int height, int weight, int hP,
			Weapon weapon, Vehicle vehicle) {
		super(name, sureName, height, weight, hP, weapon, vehicle);
		System.out.println("Character Dawid Created!");
	}

	/**
	 * pomocnicza klasa wewnetrzna
	 */
	private static final class DawidHelper {
		private static final Weapon blackSword = new White(200, 12.45, "Black",
				true, 50, "One hand");
		private static final Vehicle motocycle = new Motocycle(250, 3, "Black",
				"Kawasaki R-300", 50000, false);
		private static final Dawid INSTANCE = new Dawid("Dawid", "Paluch", 180,
				80, 100, blackSword, motocycle);
	}

	/**
	 * 
	 * @return funkcja zwraca obiekt Michal
	 */
	public static Dawid getDawidInstance() {
		return DawidHelper.INSTANCE;
	}

	@Override
	/**
	 * poruszanie 
	 */
	public void move() {
		System.out.println("Poruszanie sie Dawida");
	}

	/**
	 * nadpisana metoda jedz
	 */
	public void Drive(Vehicle vehicle) throws NullObjectException {

		if (vehicle == null)
			throw new NullObjectException();

		else {
			System.out.print("Dawid driving by ");
			if (vehicle instanceof Car)
				System.out.println("car");
			else if (vehicle instanceof Motocycle)
				System.out.println("motocycle");
		}

	}

	@Override
	public void Fight(Character character, Weapon weapon) throws UnattackableException, NullObjectException {
		if(character == null) 
			throw new NullObjectException();
		else if (character instanceof Dawid )
			throw new UnattackableException();
		else {
			System.out.print("You are fighting with " + character.getName()
					+ " using ");
			if (weapon instanceof Riffle)
				System.out.println("riffle");
			else if (weapon instanceof White)
				System.out.println("white weapon");

			weapon.Fight();
		}
		
	}

}
