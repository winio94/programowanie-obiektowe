package com.winio94.game.main;

import java.util.Arrays;

import com.winio94.game.Characters.Character;
import com.winio94.game.Characters.Dawid;
import com.winio94.game.Characters.Michal;
import com.winio94.game.Vehicles.Car;
import com.winio94.game.Vehicles.Motocycle;
import com.winio94.game.Vehicles.Vehicle;
import com.winio94.game.exception.NullObjectException;
import com.winio94.game.exception.UnattackableException;
import com.winio94.game.exception.UnpriopriateValueException;
import com.winio94.game.items.Item;
import com.winio94.game.weapons.Riffle;
import com.winio94.game.weapons.Weapon;
import com.winio94.game.weapons.White;

/**
 * 
 * @author Michal Winnicki
 * 
 */

public class MainClass {

	public static void main(String[] args) {
/*
		try {
			Character michal = Michal.getMichalInstance();
			Character dawid = Dawid.getDawidInstance();

			Vehicle auto1 = new Car(100, 10, "Czerwony", 200, "wy�cigowy", 5);
			Vehicle motor1 = new Motocycle(100, 5, "blue", "Honda", 10000, true);
			michal.Drive(motor1);
			michal.Drive(auto1);

			Riffle bronMichala = (Riffle) michal.getWeapon();
			White bronDawida = (White) dawid.getWeapon();
			bronMichala.Reload(0);

			dawid.Fight(null, bronMichala);

		} catch (NullObjectException nullObjectException) {
			nullObjectException.printStackTrace();

		} catch (UnpriopriateValueException unpriopriateValueException) {
			unpriopriateValueException.f();
			unpriopriateValueException.printStackTrace();
		} catch (UnattackableException unattackableException) {
			unattackableException.unattacble();
			unattackableException.printStackTrace();
		} finally {
			System.out.println("Blok finally wykona sie w kazdym wypadku!");
		}
		*/
		
		try {
			Item item1 = new Item("woda", new Integer(3));
			Item item2 = new Item("amunicja", new Integer(5));
			Item item3 = new Item("mapa", new Integer(1));
			Item item4 = new Item("bron", new Integer(10));
			Item item5 = new Item("czapka", new Integer(2));
			Item item6 = new Item("a", new Integer(34));
			
			Character michal = Michal.getMichalInstance();
			michal.itemAdding(item1);
			michal.itemAdding(item2);
			michal.itemAdding(item3);
			michal.itemAdding(item4);
			michal.itemAdding(item5);
			michal.itemAdding(item6);
			michal.itemAdding(item6);

			
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

}
