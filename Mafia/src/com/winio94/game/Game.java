package com.winio94.game;
/**
 * 
 * @author Michal Winnicki
 *
 */
public class Game {

	private Character character;
	/**
	 * 
	 * @param character
	 */
	public Game(Character character) {
		super();
		this.character = character;
	}
	/**
	 * 
	 * @return zwracajaca postac
	 */
	public Character getCharacter() {
		return character;
	}
	
	public void play() {
		
	}
}
