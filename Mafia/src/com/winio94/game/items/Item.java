package com.winio94.game.items;

public class Item {
	private String nazwa;
	private Integer value;

	public Item(String nazwa, Integer value) {
		this.nazwa = nazwa;
		this.value = value;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
