package com.winio94.game.items;

import java.util.HashMap;

import com.winio94.game.exception.CollectionCapacityOverflowException;

public class Backpack {
	//maksymalna pojemnosc plecaka to 5 rzeczy
	private HashMap<Integer, Item> listOfItems = new HashMap<Integer, Item>(5);

	public void addItem(Item item) throws CollectionCapacityOverflowException {
		if ((listOfItems.size() <= 5))
			listOfItems.put(item.getValue(), item);
		else throw new CollectionCapacityOverflowException();

	}
}
