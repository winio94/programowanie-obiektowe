package telewizor;

import java.io.IOException;

import com.strumien.wejscie.Cin;

/**
 * 
 * @author Michal Winnicki
 * 
 */
public class Zad1 {
	/**
	 * 
	 * @param args
	 *            tablica String Glowna funkcja w ktorej wystepuje zagniezdzone
	 *            funkcje switch
	 */
	public static void main(String[] args) {

		Cin cin = new Cin();
		int poziom = 0;
		while (true) {
			System.out.println("0 - ustawienia obrazu");
			System.out.println("1 - ustawienia glosnosc");
			System.out.println("2 - informacje");
			System.out.println("3 - strojenie kanalow");
			System.out.println("4 - ustawienie satelity");

			switch (wybierzPoziom()) {

			case 0: {
				System.out.println("0 - Jasnosc");
				System.out.println("1 - Kontrast");
				System.out.println("2 - Gamma");
				System.out.println("3 - Rozdzielczosc");
				System.out.println("4 - ustawienie zaawansowane");

				switch (wybierzPoziom()) {

				case 0: {
					System.out.println("Ustawienia jasnosci");
					break;
				}
				case 1: {
					System.out.println("Ustawienia kontrastu");
					break;
				}
				case 2: {
					System.out.println("Ustawienia gamma");
					break;
				}
				case 3: {
					System.out.println("Ustawienia rozdzielczosci");
					break;
				}
				case 4: {
					System.out.println("0 - ilosc pikseli");
					System.out.println("1 - full hd");
					System.out.println("2 - hdr");

					switch (wybierzPoziom()) {

					case 0: {
						System.out.println("wybor ilosci pikseli");
						break;
					}
					case 1: {
						System.out
								.println("opcja przeznaczona tylo dla abonamentu gold");
						break;
					}
					case 2: {
						System.out.println("sniezenie ekranu");
						break;
					}
					}
					break;
				}

				}

				break;
			}
			case 1: {
				System.out.println("0 - ilosc glosnikow");
				System.out.println("1 - efekt 3d");
				System.out.println("2 - zapetlanie dzwieku");
				switch (wybierzPoziom()) {
				case 0: {
					System.out.println("glosniki telewizora lub zewnetrzne");
					break;
				}
				case 1: {
					System.out.println("wybor efektu 3d");
					break;
				}
				case 2: {
					System.out.println("opcja wymagajaca uzycia sluchawek");
					break;
				}
				}
				break;
			}
			case 2: {
				System.out.println("0 - informacje na temat oplat");
				System.out.println("1 - dziennik telewizyjny");
				System.out.println("2 - informacje zaawansowane");

				switch (wybierzPoziom()) {
				case 0: {
					System.out.println("Oplaty w tym miesiacu : 100zl");
					break;
				}
				case 1: {
					System.out.println("wybor programu: ");
					break;
				}
				case 2: {
					System.out.println("informacje zaawansowane");
					break;
				}
				}
				break;
			}
			case 3: {
				System.out.println("0 - informacje na temat jakosci sygnalu");
				System.out.println("1 - informacje na temat sily sygnalu");
				switch (wybierzPoziom()) {
				case 0: {
					System.out.println("Jakosc sygnalu 80%");
					break;
				}
				case 1: {
					System.out.println("Sila sygnalu 40% ");						
					break;
				}
				
				
				}
			}
			break;
			case 4: {
				System.out.println("Satelita znajduje sie w polozeniu neutralnym");
				break;
			}

			}

		}

	}

	/**
	 * 
	 * @return zwracana wartosc int reprezentujaca wybrany poziom
	 */
	public static int wybierzPoziom() {
		int poziom = 0;
		try {
			poziom = Cin.cinInt("Wybor : ");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return poziom;
	}

}
