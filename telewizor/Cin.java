package com.strumien.wejscie;
 
import java.io.*;
import java.util.*;
 
/**
 * 
 * @author SB
 * @version 1.0 Klasa imitujÄca strumieÅ wejÅcia znany z jÄzyka C++ jako cin
 */
public class Cin {
	/**
	 * @param listInt
	 *            to bufor klawiatury przechowujÄcy liczby typu int
	 */
	private static ArrayList<String> listInt = new ArrayList<String>();
 
	/**
	 * Metoda cinInt sÅuÅ¼y do pobrania od uÅ¼ytkownika liczby
	 * 
	 * @param komunikat
	 *            komunikat wyÅwietlany uÅ¼ytkownikowi (co ma zrobiÄ)
	 * @return wartoÅÄ typu int wprowadzonÄ z klawiatury
	 */
	public static int cinInt(String komunikat) throws IOException {
		/**
		 * JeÅli lista nie jest pusta to znaczy, Å¼e coÅ czeka na pobranie z
		 * bufora klawiatury
		 */
		if (listInt.isEmpty()) {
			System.out.print(komunikat);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));
			// odczyt wartoÅci z strumienia
			String s = in.readLine();
			// jeÅli odczyt pusty to wywoÅujemy metodÄ ponownie
			if (s.length() == 0) {
				return cinInt(komunikat);
			} else {
				// sekwencja \\s+ biaÅe znaki (dowolna iloÅÄ) zostajÄ zastÄpione
				// jednÄ spacjÄ
				String[] wej = s.replaceAll("\\s+", " ").split(" ");
				for (int j = 0; j < wej.length; j++)
					listInt.add(wej[j]);
				/**
				 * Pobranie pierwszego elementu z listy i jego usuniÄcie
				 */
				Integer i = 0;
				try {
					i = (int)Double.parseDouble(listInt.remove(0));
 
				} catch (NumberFormatException e) {
					i = cinInt("WprowadÅº poprawnÄ liczbÄ: ");
				}
				return i;
			}
 
		} else {
			/**
			 * Pobranie pierwszego elementu z listy i jego usuniÄcie
			 */
			Integer i = 0;
			try {
				i = (int)Double.parseDouble(listInt.remove(0));
			} catch (NumberFormatException e) {
				i = cinInt("WprowadÅº poprawnÄ liczbÄ: ");
			}
			return i;
		}
	}
 
	/**
	 * @param listDouble
	 *            to bufor klawiatury przechowujÄcy liczby typu double;)
	 */
	private static ArrayList<String> listDouble = new ArrayList<String>();
 
	/**
	 * Metoda cinDouble sÅuÅ¼y do pobrania od uÅ¼ytkownika liczby typu double
	 * 
	 * @param komunikat
	 *            komunikat wyÅwietlany uÅ¼ytkownikowi (co ma zrobiÄ)
	 * @return wartoÅÄ typu double wprowadzonÄ z klawiatury
	 */
	public static double cinDouble(String komunikat) throws IOException {
		/**
		 * JeÅli lista nie jest pusta to znaczy, Å¼e coÅ czeka na pobranie z
		 * bufora klawiatury
		 */
		if (listDouble.isEmpty()) {
			System.out.print(komunikat);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));
			// odczyt wartoÅci z strumienia
			String s = in.readLine();
			// jeÅli odczyt pusty to wywoÅujemy metodÄ ponownie
			if (s.length() == 0)
				return cinDouble(komunikat);
			else {
				// sekwencja \\s+ biaÅe znaki
				String[] wej = s.replaceAll("\\s+", " ").split(" ");
				for (int j = 0; j < wej.length; j++)
					listDouble.add(wej[j]);
				/**
				 * Pobranie pierwszego elementu z listy i jego usuniÄcie
				 */
				Double i = 0.0;
				try {
					i = Double.parseDouble(listDouble.remove(0));
 
				} catch (NumberFormatException e) {
					i = cinDouble("WprowadÅº poprawnÄ liczbÄ: ");
				}
				return i;
			}
 
		} else {
			/**
			 * Pobranie pierwszego elementu z listy i jego usuniÄcie
			 */
			Double i = 0.0;
			try {
				i = Double.parseDouble(listDouble.remove(0));
			} catch (NumberFormatException e) {
				i = cinDouble("");
			}
			return i;
		}
	}
 
	/**
	 * @param listString
	 *            to bufor klawiatury przechowujÄcy text bez biaÅych znakÃ³w ;)
	 */
	private static ArrayList<String> listString = new ArrayList<String>();
 
	/**
	 * Metoda cinString sÅuÅ¼y do pobrania od uÅ¼ytkownika ciÄgu znakÃ³w
	 * 
	 * @param komunikat
	 *            komunikat wyÅwietlany uÅ¼ytkownikowi (co ma zrobiÄ)
	 * @return wartoÅÄ typu string wprowadzonÄ z klawiatury
	 */
	public static String cinString(String komunikat) throws IOException {
		/**
		 * JeÅli lista nie jest pusta to znaczy, Å¼e coÅ czeka na pobranie z
		 * bufora klawiatury
		 */
		if (listString.isEmpty()) {
			System.out.print(komunikat);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));
			// odczyt wartoÅci z strumienia
			String s = in.readLine();
			// jeÅli odczyt pusty to wywoÅujemy metodÄ ponownie
			if (s.length() == 0)
				return cinString(komunikat);
			else {
				// sekwencja \\s+ biaÅe znaki
				String[] wej = s.replaceAll("\\s+", " ").split(" ");
				for (int j = 0; j < wej.length; j++)
					listString.add(wej[j]);
				/**
				 * Pobranie pierwszego elementu z listy i jego usuniÄcie
				 */
				return listString.remove(0);
			}
 
		} else {
			/**
			 * Pobranie pierwszego elementu z listy i jego usuniÄcie
			 */
			return listString.remove(0);
		}
	}
 
	/**
	 * @param listStringText
	 *            to bufor klawiatury przechowujÄcy text wpisany z klawiatury,
	 *            jaki znak dzieli tekst wprowadza uÅ¼ytkownik
	 */
	private static ArrayList<String> listStringText = new ArrayList<String>();
 
	/**
	 * Metoda cinString sÅuÅ¼y do pobrania od uÅ¼ytkownika ciÄgu znakÃ³w
	 * 
	 * @param komunikat
	 *            komunikat wyÅwietlany uÅ¼ytkownikowi (co ma zrobiÄ)
	 * @param znak
	 *            separator wczytywanych znakÃ³w
	 * @return wartoÅÄ typu string wprowadzonÄ z klawiatury
	 */
	public static String cinString(String komunikat, String znak)
			throws IOException {
		/**
		 * JeÅli lista nie jest pusta to znaczy, Å¼e coÅ czeka na pobranie z
		 * bufora klawiatury
		 */
		if (listStringText.isEmpty()) {
			System.out.print(komunikat);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));
			// odczyt wartoÅci z strumienia
			String s = in.readLine();
			// jeÅli odczyt pusty to wywoÅujemy metodÄ ponownie
			if (s.length() == 0)
				return cinString(komunikat);
			else {
				// sekwencja \\s+ biaÅe znaki
				String[] wej = s.replaceAll("\\s+", " ").split(znak);
				for (int j = 0; j < wej.length; j++)
					listStringText.add(wej[j]);
				/**
				 * Pobranie pierwszego elementu z listy i jego usuniÄcie
				 */
				return listStringText.remove(0);
			}
 
		} else {
			/**
			 * Pobranie pierwszego elementu z listy i jego usuniÄcie
			 */
			return listStringText.remove(0);
		}
	}
 
	/**
	 * @param listCharacter
	 *            to bufor klawiatury przechowujÄcy text bez biaÅych znakÃ³w ;)
	 */
	private static ArrayList<Character> listCharacter = new ArrayList<Character>();
 
	/**
	 * Metoda cinCharacter sÅuÅ¼y do pobrania od uÅ¼ytkownika znakÃ³w char
	 * 
	 * @param komunikat
	 *            komunikat wyÅwietlany uÅ¼ytkownikowi (co ma zrobiÄ)
	 * @return wartoÅÄ typu char wprowadzonÄ z klawiatury
	 */
	public static Character cinCharacter(String komunikat) throws IOException {
		/**
		 * JeÅli lista nie jest pusta to znaczy, Å¼e coÅ czeka na pobranie z
		 * bufora klawiatury
		 */
		if (listCharacter.isEmpty()) {
			System.out.print(komunikat);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));
			// odczyt wartoÅci z strumienia
			String s = in.readLine();
			// jeÅli odczyt pusty to wywoÅujemy metodÄ ponownie
			if (s.length() == 0)
				return cinCharacter(komunikat);
			else {
				// sekwencja \\s+ biaÅe znaki
				String wej = s.replaceAll("\\s+", "");
				for (int j = 0; j < wej.length(); j++)
					listCharacter.add(wej.charAt(j));
				/**
				 * Pobranie pierwszego elementu z listy i jego usuniÄcie
				 */
				return listCharacter.remove(0);
			}
 
		} else {
			/**
			 * Pobranie pierwszego elementu z listy i jego usuniÄcie
			 */
			return listCharacter.remove(0);
		}
	}
 
	public static void main(String[] args) throws IOException {
		PrintWriter out = new PrintWriter(System.out, true);
		// przykÅad uÅ¼ycia
		try {
			Integer k = cinInt("wprowadÅº liczbÄ i: ");
			System.out.println(k + 15);
			k = cinInt("wprowadÅº liczbÄ i: ");
			System.out.println(k + 15);
			Double d = cinDouble("wprowadÅº liczbÄ d: ");
			System.out.println(d + 15);
			String s = cinString("wprowadÅº tekst: ", "\n");
			System.out.println(s);
			s = cinString("wprowadÅº tekst: ", "X");
			System.out.println(s);
			Character c = cinCharacter("wprowadÅº znak: ");
			System.out.println(c);
			c = cinCharacter("wprowadÅº znak: ");
			System.out.println(c);
			c = cinCharacter("wprowadÅº znak: ");
			System.out.println(c);
			c = cinCharacter("wprowadÅº znak: ");
			System.out.println(c);
		} catch (IOException e) {
			out.println("BÅÄd: " + e);
		} catch (NumberFormatException e) {
			out.println("BÅÄd: " + e);
		}
	}
} // /:~