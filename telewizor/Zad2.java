package telewizor;

import java.util.Random;

/**
 * 
 * @author Michal Winnicki
 *
 */
public class Zad2 {
	
	/**
	 * Glowna klasa w ktorej znajduja sie rozwiazania do zadan 2 i 3
	 * @param args  tablica lancuchÃ³w
	 */
	public static void main(String[] args) {

		int[] tab = new int[10];
		wypelnij(tab);
		wyswietl(tab);

		System.out.println("\n\n");

		sortuj(tab);
		wyswietl(tab);

		sprawdz(tab);
		
	}
	/**
	 * 
	 * @param t tablica ktora jest zaiinicjalizowana pseudolosowymi 
	 * liczbami calkowitymi sorotwana malejaco
	 * sortowanie przez wybor
	 */
	public static void sortuj(int[] t) {
		int min = 0, temp = 0;
		for (int i = 0; i < t.length - 1; i++) {
			min = i;
			for (int j = i + 1; j < t.length; j++)
				if (t[j] > t[min])
					min = j;
			temp = t[min];
			t[min] = t[i];
			t[i] = temp;
		}
	}
	/**
	 * 
	 * @param t tablica, ktora wypelniamy liczbami pseudolosowymi
	 */
	public static void wypelnij(int[] t) {

		Random generator = new Random();
		for (int i = 0; i < t.length; i++)
			t[i] = generator.nextInt(300);
	}
	/**
	 * 
	 * @param n liczba calkowita, ktora jets sprawdzana czy jest pierwsza
	 * @return prawda jesli liczb ajest pierwsza, w przeciwnym razie falsz
	 */
	public static boolean czyPierwsza(int n) {

		double r = Math.sqrt(n);
		int i = 2;

		while (i <= r)
			if ((n % i++) == 0)
				return false;
			else if(n == 0)
				return false;
		return true;

	}
	/**
	 * 
	 * @param t tablica ktora zostaje wyswietlona
	 */
	public static void wyswietl(int[] t) {
		for (int j = 0; j < t.length; j++)
			System.out.print(t[j] + " ");
	}
	/**
	 * 
	 * @param t tablica, ktorrej elementy sa sprawdzane(czy sa pierwse) w funkcji czyPierwsza
	 */
	public static void sprawdz(int[] t) {
		int licznik = 0;
		System.out.println();
		for (int i = 0; i < t.length; i++) {
			if(czyPierwsza(t[i])) {
				licznik++;
				System.out.println("Liczba " + t[i] + " jest pierwsza");
			}
		}
		System.out.println("Znaleziono " + licznik + " liczb pierwszych");
	}
}
