/**
 * Klasa g�owna
 * @author Micha� Winnicki
 */
public class Kalkulator {
	
	/**
	 * W metodzie podawane jest wyrazenie oraz generowane sa wyniki
	 */
	public static void main(String[] args) {
		
		
		 //dwa znaki musza byc oddzielone spacja
		String [] wyrazenie = "( 7 + 31 ) * ( 5 - 2 ) ^ 2 =".split(" ");
		String wyrazenieONP = konwertuj_na_ONP(wyrazenie);
		
	
		System.out.print("Podane wyrazenie : ");
		for(String x : wyrazenie)
			System.out.print(x + " ");
		System.out.println("\nwyrazenie w ONP : " + wyrazenieONP);
		String wynik = Oblicz(wyrazenieONP);
		
		System.out.println("Wynik = " + wynik);
	}
	/**
	 * Konwertuje wyrazenie w postaci infiksowej do wyrazenia ONP
	 * @param wyr Tablica przechowywujaca wyrazenie w notacji infiksowej
	 * @return Wyrazenie w notacji ONP
	 */
	public static String konwertuj_na_ONP (String[] wyr) {
		Stack stos = new Stack(wyr.length);
		stos.init_stack();
		
		String temp_wynik = "";
		String symbol;
		System.out.println("MICHA� WINNICKI\n\t\t\tSTART KONWERSJI\n");
		for(int i = 0 ; i < wyr.length; i++) {
			
			symbol = wyr[i];

			if(isOperator(symbol) == false) {
				temp_wynik += symbol + " ";//jesli liczba to odkladam na wyjscie
			}
			else if(symbol.equals("(")) {//jezeli lewy nawias
				stos.push(symbol);
			}
			else if(symbol.equals(")")) {//jesli prawy nawias to pobieram elementy ze stosu do napotkania lewego nawiasu
				while( !stos.peek_element().equals("("))
					temp_wynik += stos.pop() + " ";
				stos.pop();
			}
			else if(symbol.equals("^"))
				stos.push(symbol);
			else if(symbol.equals("*") || symbol.equals("/")) {
				while((stos.peek_element().equals("*")) || (stos.peek_element().equals("/") ) || (stos.peek_element().equals("^") )  ) 
					temp_wynik += (stos.pop() + " ");
				stos.push(symbol);
			}
			else if(symbol.equals("+") || symbol.equals("-")) {
				while(stos.peek_element().equals("+") || stos.peek_element().equals("-") 
						|| stos.peek_element().equals("*") || stos.peek_element().equals("/")
						|| stos.peek_element().equals("^")) 
					temp_wynik += stos.pop() + " ";
				stos.push(symbol);
			}
			else if(symbol.equals("=")) {
				while(!stos.isempty())
					temp_wynik += stos.pop() + " ";
				temp_wynik += symbol;
			}
		}
		
		return temp_wynik;
	}
	
	/**
	 * Zwraca prawde jezeli symbol jest operatorem, falsz w przeciwnym razie
	 * @param n operator lub liczba
	 * @return wartosc logiczna
	 */
	public static boolean isOperator(String n) {
		String[] operatory = {"(", ")", "=", "+", "-", "*", "/", "^"};
		for(String x : operatory) {
			if(x.equals(n))
				return true;
		}
		return false;
	}
	
	/**
	 * Wyliczenie wartosci wyrazenia podanego w notacji ONP
	 * @param wyrONP wyrazenie(lancuch) w notacji ONP
	 * @return wynik w postaci lancucha
	 */
	public static String Oblicz(String wyrONP) {
		Stack stos2 = new Stack(wyrONP.length());
		stos2.init_stack();
		
		String [] wyr = wyrONP.split(" ");
		
		double var1 = 0.0, var2 = 0.0;
		String symbol;
		String temp_wynik = " ";
		
		for(int i = 0 ; i < wyr.length; i++) {

			symbol = wyr[i];
			
			if(isOperator(symbol) == false) {
				stos2.push(symbol);;//jesli liczba to odkladam na wyjscie
			}
			else { //w przeciwnym razie wykonuje dzialanie na dwoch ostatnich iczbach stosu 
				if(symbol.equals("=")) {
					return temp_wynik = stos2.pop();
				}
				if(!stos2.isempty()) {
					var1 = Double.parseDouble(stos2.pop());
					var2 = Double.parseDouble(stos2.pop());
				}
				switch(symbol) {
				case "+" :
					var2 += var1;
					break;
				case "-" :
					var2 -= var1;
					break;
				case "*" :
					var2 *= var1;
					break;
				case "/" :
					var2 /= var1;
					break;
				case "^" :
					var2 = Math.pow(var2, var1);
					break;
				}
				stos2.push(Double.toString(var2));
			}
		}
		return temp_wynik;
	}
}
