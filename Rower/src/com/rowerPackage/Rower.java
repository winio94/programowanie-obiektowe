package com.rowerPackage;
import com.resources.HamulceRoweru;
import com.resources.KierownicaRoweru;
import com.resources.Marka;
import com.resources.RamaRoweru;
import com.resources.SiodelkoRoweru;

/**
 * 
 * @author Michal Winnicki
 *
 */
public class Rower {
	private HamulceRoweru hamulce;
	private KierownicaRoweru kierownica;
	private RamaRoweru rama;
	private SiodelkoRoweru siodelko;
	private Marka marka;
	private double cena;

	/**
	 * 
	 * @param hamulce
	 *            hamulce roweru
	 * @param kierownica
	 *            kierownica roweru
	 * @param rama
	 *            rama roweru
	 * @param siodelko
	 *            siodelko roweru
	 * @param marka
	 *            marka roweru
	 */
	public Rower(HamulceRoweru hamulce, KierownicaRoweru kierownica,
			RamaRoweru rama, SiodelkoRoweru siodelko, Marka marka) {
		super();
		this.hamulce = hamulce;
		this.kierownica = kierownica;
		this.rama = rama;
		this.siodelko = siodelko;
		this.marka = marka;
	}

	/**
	 * 
	 * @param marka
	 *            marka roweru
	 */
	public Rower(Marka marka) {
		super();
		this.marka = marka;
	}

	/**
	 * 
	 * @param hamulce
	 *            hamulce roweru
	 * @param kierownica
	 *            kierownica roweru
	 * @param rama
	 *            rama roweru
	 * @param siodelko
	 *            siodelko roweru
	 */
	public Rower(HamulceRoweru hamulce, KierownicaRoweru kierownica,
			RamaRoweru rama, SiodelkoRoweru siodelko) {
		super();
		this.hamulce = hamulce;
		this.kierownica = kierownica;
		this.rama = rama;
		this.siodelko = siodelko;
	}

	/**
	 * 
	 * @param rower1
	 *            obiekt rower
	 * @param rower2
	 *            obiekt rower
	 */
	public void porownaj(Rower rower1, Rower rower2) {
		System.out.println(rower1);
		System.out.println(rower2);
	}

	/**
	 * 
	 * @param marka1
	 *            marka roweru
	 * @param marka2
	 *            marka roweru
	 */
	public void porownaj(Marka marka1, Marka marka2) {
		System.out.println("Marka roweru1 : " + marka1);
		System.out.println("Marka roweru2 : " + marka2);
	}

	/**
	 * 
	 * @param cena1
	 *            cena pierwszego roweru
	 * @param cena2
	 *            cena drugiego roweru
	 */
	public void porownaj(double cena1, double cena2) {
		if (cena1 > cena2)
			System.out.println("Pierwszy rower jest drozszy od drugiego");
		else
			System.out.println("Drugi rower jest drozszy");
	}

	/**
	 * 
	 * @param kierownica
	 *            kierownica roweru
	 */
	public void napraw(KierownicaRoweru kierownica) {
		System.out.println("Naprawianie kierownicy");
	}

	/**
	 * 
	 * @param marka
	 *            marka roweru
	 */
	public void napraw(Marka marka) {
		System.out.println("Naprawianie wylacznie pod wzgedem marki");
	}

	/**
	 * 
	 * @param rama
	 *            rama roweru
	 */
	public void napraw(RamaRoweru rama) {
		System.out.println("Naprawianie ramy roweru");
	}

	/**
	 * 
	 * @param siodelko
	 *            siodelko roweru
	 */
	public void napraw(SiodelkoRoweru siodelko) {
		System.out.println("Naprawianie siodelka");
	}

	/**
	 * 
	 * @param hamulce
	 *            hamulce roweru
	 */
	public void napraw(HamulceRoweru hamulce) {
		System.out.println("Naprawianie hamulcow");
	}

	/**
	 * 
	 * @return cena roweru
	 */
	public double getCena() {
		return cena;
	}

	/**
	 * ustawianie ceny roweru
	 */
	public void setCena() {
		this.cena = this.kierownica.getCena() + this.rama.getCena()
				+ this.hamulce.getCena() + this.siodelko.getCena();
	}

	/**
	 * funkcja wyswietlajaca informacje o rowerze
	 */
	public void wyswietl() {
		System.out.println("Cena roweru :" + getCena());
		if (this.hamulce != null) {
			System.out.println("Cena hamulcow :" + this.hamulce.getCena());
			System.out.println("marka hamulcow :" + this.hamulce.getMarka());
			System.out.println("sila hamowania :"
					+ this.hamulce.getSilaHamowaniaWNewtonach());
		}

		if (this.kierownica != null) {
			System.out.println("Cena kierownicy :" + this.kierownica.getCena());
			System.out.println("Maksymalny skret :"
					+ this.kierownica.getMaksymalnySkret());
		}

		if (this.rama != null) {
			System.out.println("Cena ramy :" + this.rama.getCena());
			System.out.println("Przeznaczenie ramy :"
					+ this.rama.getPrzeznaczenie());
			System.out.println("Wielkosc ramy :" + this.rama.getWielkosc());
		}

		if (this.siodelko != null) {
			System.out.println("Cena siodelka :" + this.siodelko.getCena());
			System.out.println("Marka siodelka :" + this.siodelko.getMarka());
			System.out.println("Rodzaj obicia siodelka :"
					+ this.siodelko.getRodzajObicia());
		}

	}

	/**
	 * 
	 * @param marka
	 *            - marka roweru
	 */
	public void wyswietl(Marka marka) {
		this.marka.show();
	}
}
