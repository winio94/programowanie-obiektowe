package com.main;

import com.resources.HamulceRoweru;
import com.resources.KierownicaRoweru;
import com.resources.Kona;
import com.resources.Marka;
import com.resources.RamaRoweru;
import com.resources.SiodelkoRoweru;
import com.resources.Treck;
import com.rowerPackage.Rower;

/**
 * 
 * @author Michal Winnicki
 *
 */
public class MainClass {
	/**
	 * Glowna klasa w ktorej towrzone sa obiekty
	 * 
	 * @param args
	 *            tablica Sting
	 */
	public static void main(String[] args) {

		HamulceRoweru hamulce1 = new HamulceRoweru(100.0, "Shimano", 120);
		HamulceRoweru hamulce2 = new HamulceRoweru(459.99, "Boxer", 300);

		KierownicaRoweru kierownica1 = new KierownicaRoweru(250.49, 170);
		KierownicaRoweru kierownica2 = new KierownicaRoweru(159.99, 100);

		Marka marka1 = new Treck("5300");
		Marka marka2 = new Kona("k4");

		RamaRoweru rama1 = new RamaRoweru(16, 500, "gorski");
		RamaRoweru rama2 = new RamaRoweru(12, 800, "kolarka");

		SiodelkoRoweru siodelko1 = new SiodelkoRoweru("shimano", 100.99,
				"skurzane");
		SiodelkoRoweru siodelko2 = new SiodelkoRoweru("kuma", 55.99, "sztuczne");

		Rower rower0 = new Rower(hamulce1, kierownica1, rama1, siodelko1,
				marka1);
		rower0.setCena();
		rower0.wyswietl();

		System.out.println("\n\n");

		Rower rower1 = new Rower(marka1);
		rower1.wyswietl(marka1);
	}

}
