package com.resources;
/**
 * 
 * @author Michal Winnicki klasa opisujaca hamulce rowerowe posiadajaca trzy
 *         pola reprezentujace odpowiednio cene, marke oraz sile hamowania
 *
 */
public class HamulceRoweru {
	private double cena;
	private String marka;
	private int silaHamowaniaWNewtonach;

	/**
	 * konstruktor parametry reprezentujace cechy hamulcow
	 * 
	 * @param cena
	 * @param marka
	 * @param silaHamowaniaWNewtonach
	 */
	public HamulceRoweru(double cena, String marka, int silaHamowaniaWNewtonach) {
		super();
		this.cena = cena;
		this.marka = marka;
		this.silaHamowaniaWNewtonach = silaHamowaniaWNewtonach;
	}

	/**
	 * 
	 * @return zwracana cena hamulcow
	 */
	public double getCena() {
		return cena;
	}

	/**
	 * 
	 * @return zwracana marka hamulcow
	 */
	public String getMarka() {
		return marka;
	}

	/**
	 * 
	 * @returnzwracana wielkosc reprezentujaca sile hamowania hamulcow
	 */
	public int getSilaHamowaniaWNewtonach() {
		return silaHamowaniaWNewtonach;
	}
}
