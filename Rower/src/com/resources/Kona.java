package com.resources;

/**
 * 
 * @author Michal Winnicki
 *
 */
public class Kona extends Marka {
	/**
	 * kontruktor
	 * 
	 * @param model
	 *            - model marki
	 */
	public Kona(String model) {
		super(model);
	}

	/**
	 * funkcja wysietlajaca model danej marki
	 */
	public void show() {
		System.out.println("Model " + this.model);
	}
}
