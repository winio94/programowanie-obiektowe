package com.resources;

/**
 * 
 * @author Michal Winnicki
 *
 */
public class Monghoose extends Marka {
	/**
	 * kontruktor
	 * 
	 * @param model
	 *            - model konretnej marki roweru
	 */
	public Monghoose(String model) {
		super(model);
	}

	/**
	 * wyswietlanie informacji na temat modelu
	 */
	public void show() {
		System.out.println("Model " + this.model);
	}
}
