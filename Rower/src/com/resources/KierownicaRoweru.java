package com.resources;
/**
 * 
 * @author Michal Winnicki
 *
 */
public class KierownicaRoweru {
	private double cena;
	private double maksymalnySkret;

	/**
	 * 
	 * @param cena
	 *            parametr opisujacy cene kierownicy
	 * @param maksymalnySkret
	 *            parametr opisujacy maksymalny mozliwy kat skretu
	 */
	public KierownicaRoweru(double cena, double maksymalnySkret) {
		super();
		this.cena = cena;
		this.maksymalnySkret = maksymalnySkret;
	}

	/**
	 * funkcja zwracajaca cene kierownicy
	 * 
	 * @return cena kierownicy
	 */
	public double getCena() {
		return cena;
	}

	/**
	 * 
	 * @return wielkosc opisujaca maksymalny skret
	 */
	public double getMaksymalnySkret() {
		return maksymalnySkret;
	}
}
