package com.resources;

/**
 * 
 * @author Michal Winnicki
 *
 */
public class Treck extends Marka {
	/**
	 * 
	 * @param model
	 *            parametr opisujacy model danej marki roweru
	 */
	public Treck(String model) {
		super(model);
		// TODO Auto-generated constructor stub
	}

	/**
	 * funkcja opisujaca model
	 */
	public void show() {
		System.out.println("Model " + this.model);
	}
}
