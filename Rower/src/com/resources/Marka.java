package com.resources;
/**
 * 
 * @author Michal Winnicki
 *
 */
public abstract class Marka {
	protected String model;

	/**
	 * konstruktor
	 * 
	 * @param model
	 *            - model konkrenej marki
	 */
	public Marka(String model) {
		this.model = model;
	}

	/**
	 * abstrakcyjna metoda wyswietlajaca informacje na temat modelu danej marki
	 */
	public abstract void show();
}
