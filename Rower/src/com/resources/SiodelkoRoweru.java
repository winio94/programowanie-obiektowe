package com.resources;
/**
 * 
 * @author Michal Winnicki
 *
 */
public class SiodelkoRoweru {
	private String marka;
	private double cena;
	private String rodzajObicia;

	/**
	 * konstruktor ustawiajacy odpowiednio marke, cene oraz rodzaj obicia
	 * obiektu SiodelkoRoweru
	 * 
	 * @param marka
	 *            -marka roweru
	 * @param cena
	 *            -cena roweru
	 * @param rodzajObicia
	 *            - rodzaj obicia
	 */
	public SiodelkoRoweru(String marka, double cena, String rodzajObicia) {
		super();
		this.marka = marka;
		this.cena = cena;
		this.rodzajObicia = rodzajObicia;
	}

	/**
	 * 
	 * @return marka siodelka
	 */
	public String getMarka() {
		return marka;
	}

	/**
	 * 
	 * @return cena siodelka
	 */
	public double getCena() {
		return cena;
	}

	/**
	 * 
	 * @return lancuch reprezentujacy rodzaj obicia siodelka
	 */
	public String getRodzajObicia() {
		return rodzajObicia;
	}
}
