package com.resources;
/**
 * 
 * @author Michal Winnicki
 *
 */
public class RamaRoweru {
	private int wielkosc;
	private double cena;
	private String przeznaczenie;

	/**
	 * konstruktor
	 * 
	 * @param wielkosc
	 *            - wielkosc ramy
	 * @param cena
	 *            - cena ramy
	 * @param przeznaczenie
	 *            - przeznaczenie ramy
	 */
	public RamaRoweru(int wielkosc, double cena, String przeznaczenie) {
		super();
		this.wielkosc = wielkosc;
		this.cena = cena;
		this.przeznaczenie = przeznaczenie;
	}

	/**
	 * 
	 * @return zwracana wielkosc ramy
	 */
	public int getWielkosc() {
		return wielkosc;
	}

	/**
	 * 
	 * @return cena ramy
	 */
	public double getCena() {
		return cena;
	}

	/**
	 * 
	 * @return przeznaczenie uzytku ramy
	 */
	public String getPrzeznaczenie() {
		return przeznaczenie;
	}

}
